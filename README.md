youtube-dl alternative for invidious (https://invidiou.site)

Roadmap
- [x] video download
- [x] subtitle download 
- [x] download progress indicator
- [ ] audio download support
- [ ] format option ( youtube-dl's -F & -f equivalent)
- [ ] Resume Broken Download support
- [ ] subtitle language option
- [ ] subtitle format option
- [ ] playlist support
- [ ] playlist name template option