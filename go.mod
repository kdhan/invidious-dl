module gitlab.com/kdhan/invidious-dl

go 1.13

require (
	github.com/asticode/go-astisub v0.3.0
	github.com/cavaliercoder/grab v2.0.0+incompatible
	github.com/schollz/progressbar/v3 v3.3.4
)
