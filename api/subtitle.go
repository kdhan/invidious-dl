package api

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"github.com/asticode/go-astisub"
)

// /api/v1/captions/:id[&lang=<lang> | &tlang=<lang>]

// DownloadSubtitle downloads subtitle of a given LanguageCode and subtitle format to filepath
func (details *Video) DownloadSubtitle(code, filepath, format string) error {
	vttURL := ""
	for _, caption := range details.Captions {
		if caption.LanguageCode == code {
			vttURL = caption.URL
			break
		}
	}
	if vttURL == "" {
		return fmt.Errorf("Invalid subtitle language code: %s", code)
	}
	tmpVtt, err := ioutil.TempFile(os.TempDir(), "*.vtt")
	if err != nil {
		return err
	}
	vttdownURL, err := details.Instance.GenerateURL(vttURL)
	if err != nil {
		return err
	}
	err = downloadFile(vttdownURL, tmpVtt.Name())
	if err != nil {
		return err
	}
	defer os.Remove(tmpVtt.Name())
	if strings.ToLower(code) == "vtt" {
		_, err := copyFile(tmpVtt.Name(), filepath)
		return err
	}

	vtt, err := astisub.OpenFile(tmpVtt.Name())
	if err != nil {
		return err
	}

	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer out.Close()
	switch strings.ToLower(format) {
	case "srt":
		vtt.WriteToSRT(out)
		break
	default:
		return fmt.Errorf("Invalid subtitle format: %s", format)
	}
	return nil
}
