package api

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

// Video contains details of a video in a stream
type Video struct {
	Title           string           `json:"title"`
	VideoID         string           `json:"videoId"`
	AdaptiveFormats []AdaptiveFormat `json:"adaptiveFormats"`
	FormatStreams   []FormatStream   `json:"formatStreams"`
	Captions        []Caption        `json:"captions"`
	Instance        *Instance        `json:"-"`
}

type AdaptiveFormat struct {
	Index        string `json:"index"`
	Bitrate      string `json:"bitrate"`
	Init         string `json:"init"`
	URL          string `json:"url"`
	Itag         string `json:"itag"`
	Type         string `json:"type"`
	Clen         string `json:"clen"`
	Lmt          string `json:"lmt"`
	Fps          int    `json:"fps"`
	Container    string `json:"container"`
	Encoding     string `json:"encoding"`
	QualityLabel string `json:"qualityLabel"`
	Resolution   string `json:"resolution"`
}

// FormatStream contains details of a single stream
type FormatStream struct {
	URL          string `json:"url"`
	Itag         string `json:"itag"`
	Type         string `json:"type"`
	Quality      string `json:"quality"`
	Container    string `json:"container"`
	Encoding     string `json:"encoding"`
	QualityLabel string `json:"qualityLabel"`
	Resolution   string `json:"resolution"`
	Size         string `json:"size"`
}

// Caption contains details of each caption
type Caption struct {
	Label        string `json:"label"`
	LanguageCode string `json:"languageCode"`
	URL          string `json:"url"`
}

// NewVideo fetches details of a video from invidious api and returns it
func (inst *Instance) NewVideo(id string) (*Video, error) {
	apiURL, err := inst.GenerateURL("/api/v1/videos/" + id)
	if err != nil {
		return nil, err
	}
	resp, err := http.Get(apiURL)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	var details Video
	err = json.NewDecoder(resp.Body).Decode(&details)
	if err != nil {
		return nil, err
	}
	details.Instance = inst
	return &details, nil
}

// DownloadFile downloads video stream of a given Itag to filepath
func (details *Video) DownloadFile(tag, filepath string) error {
	fileURL, err := details.Instance.GenerateURL(fmt.Sprintf("/latest_version?id=%s&itag=%s", details.VideoID, tag))
	if err != nil {
		return err
	}
	log.Println("Video URL:", fileURL)
	return downloadFile(fileURL, filepath)
}
