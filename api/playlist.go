// /api/v1/playlists/:plid

package api

import (
	"encoding/json"
	"net/http"
)

type Playlist struct {
	Title      string          `json:"title"`
	PlaylistID string          `json:"playlistId"`
	VideoCount int             `json:"videoCount"`
	Videos     []PlaylistVideo `json:"videos"`
}
type PlaylistVideo struct {
	Title         string `json:"title"`
	VideoID       string `json:"videoId"`
	Index         int    `json:"index"`
	LengthSeconds int    `json:"lengthSeconds"`
}

// NewPlaylist fetches details of a playlist from invidious api and returns it
func (inst *Instance) NewPlaylist(id string) (*Playlist, error) {
	apiURL, err := inst.GenerateURL("/api/v1/playlists/" + id)
	if err != nil {
		return nil, err
	}
	resp, err := http.Get(apiURL)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	var playlist Playlist
	err = json.NewDecoder(resp.Body).Decode(&playlist)
	if err != nil {
		return nil, err
	}
	return &playlist, nil
}
