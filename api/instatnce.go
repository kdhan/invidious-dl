package api

import (
	"fmt"
	"net/url"
)

const (
	defaultHost   = "invidious.site"
	defaultScheme = "https"
)

type Instance struct {
	host   string
	scheme string
}

func NewInstance(uri string) (*Instance, error) {
	u, err := url.Parse(uri)
	if err != nil {
		return nil, err
	}
	scheme := u.Scheme
	if scheme == "" {
		scheme = defaultScheme
	}
	return &Instance{
		host:   u.Host,
		scheme: scheme,
	}, nil
}

var DefaultInstance = &Instance{
	host:   defaultHost,
	scheme: defaultScheme,
}

func (inst *Instance) GenerateURL(urlPath string) (string, error) {
	// u, err := url.Parse(fmt.Sprintf("%s://%s", inst.scheme, inst.host))
	// if err != nil {
	// 	return "", err
	// }
	// u.Path = path.Join(u.Path, urlPath)
	// return u.String(), nil
	return fmt.Sprintf("%s://%s%s", inst.scheme, inst.host, urlPath), nil
}
