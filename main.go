package main

import (
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/kdhan/invidious-dl/api"
)

// ErrMatchNotFound is used when a search fails to find a match
var ErrMatchNotFound = errors.New("No match found")

var instance = api.DefaultInstance

// https://invidiou.site/watch?v=WZ6NdfwlKIQ
// https://invidiou.site/watch?v=DHLRj1LaPiQ

func main() {

	var formatList bool
	var format int
	var audio bool
	// subitle format
	// naming template
	// subtitle
	var url string

	flag.BoolVar(&formatList, "-F", false, "Lists all of the available formats")
	flag.IntVar(&format, "-f", 0, "format tag for video stream")
	flag.BoolVar(&audio, "audio", false, "do not download video file")
	flag.StringVar(&url, "url", "", "video/playlist URL")

	flag.Parse()
	if url == "" {
		if len(flag.Args()) == 0 {
			// help !!!
			// flag.ErrHelp
			log.Printf("Usage: %s [-F] [-f=] [-audio] <url/video-id>\n", os.Args[0])
			os.Exit(1)
		} else {
			url = flag.Args()[0]
		}
	}
	videoID, err := findVideoID(url)
	if videoID != "" {
		if audio {
			downloadAudio(videoID)
		} else {
			downloadVideo(videoID)
		}
		return
	}
	if err != ErrMatchNotFound {
		log.Fatal(err)
	}

	playListID, err := findPlaylistID(url)
	if playListID != "" {
		downloadPlaylist(playListID)
		return
	}

	log.Fatal(err)
}

func findVideoID(url string) (string, error) {
	// v=([a-zA-Z0-9_-]+)|\/([a-zA-Z0-9_-]+$)
	return findFirstSubmatch(url, []string{
		`v=([a-zA-Z0-9_-]+)`,
		`\/([a-zA-Z0-9_-]+$)`,
	})
}

func findPlaylistID(url string) (string, error) {
	// playlist=([a-zA-Z0-9_-]+)
	return findFirstSubmatch(url, []string{
		`list=([a-zA-Z0-9_-]+)`,
	})
}

func findFirstSubmatch(s string, regexPatterns []string) (string, error) {
	for _, pattern := range regexPatterns {
		re, err := regexp.Compile(pattern)
		if err != nil {
			return "", err
		}
		matches := re.FindStringSubmatch(s)
		if len(matches) > 1 {
			return matches[1], nil
		}
	}

	return "", ErrMatchNotFound
}

func downloadPlaylist(id string) error {
	playlist, err := instance.NewPlaylist(id)
	if err != nil {
		return err
	}
	log.Println("Downloading playlist", playlist.Title)
	for index, video := range playlist.Videos {
		vid, err := instance.NewVideo(video.VideoID)
		if err != nil {
			log.Println(err)
			continue
		}

		log.Println(video.Title)

		bestStream := vid.FormatStreams[0]
		bestItag := 0
		// best video stream
		if len(vid.FormatStreams) == 0 {
			if len(vid.AdaptiveFormats) == 0 {
				log.Println(errors.New("No video streams found"))
				continue
			}
			for _, format := range vid.AdaptiveFormats {
				log.Println(format.Type)
			}
			continue
		} else {
			for _, stream := range vid.FormatStreams {
				itag, err := strconv.Atoi(stream.Itag)
				if err != nil {
					continue
				}
				if itag > bestItag {
					bestStream = stream
				}
			}
		}

		videoItag := bestStream.Itag
		videoFile := filenameEscape(fmt.Sprintf("%02d_%s_%s.%s", index, vid.Title, vid.VideoID, bestStream.Container))
		videoURL, err := instance.GenerateURL(fmt.Sprintf("/latest_version?id=%s&itag=%s", vid.VideoID, videoItag))
		if err != nil {
			return err
		}
		fmt.Println(vid.Title)
		fmt.Printf("wget -c '%s' -O\"%s\"\n", videoURL, strings.ReplaceAll(videoFile, "\"", "\\\""))

		subtitleLangCode := "en"
		srtFile := filenameEscape(fmt.Sprintf("%02d_%s_%s.%s.srt", index, video.Title, video.VideoID, subtitleLangCode))
		log.Println("Downloading subtitle...")
		vid.DownloadSubtitle(subtitleLangCode, srtFile, "srt")
		// srtFile, _ := ioutil.TempFile(os.TempDir(), "*.srt")
		// err = vid.DownloadSubtitle(subtitleLangCode, srtFile.Name(), "srt")
		// var cmd *exec.Cmd
		// if err != nil {
		// 	cmd = exec.Command("mpv", fmt.Sprintf("--title=%s", video.Title), videoURL)
		// } else {
		// 	cmd = exec.Command("mpv", fmt.Sprintf("--title=%s", video.Title), fmt.Sprintf("--sub-file=%s", srtFile.Name()), videoURL)
		// }
		// cmd.Run()
	}
	return nil
}

func downloadAudio(id string) error {
	video, err := instance.NewVideo(id)
	if err != nil {
		return err
	}

	log.Println(video.Title)

	if len(video.FormatStreams) == 0 {
		return errors.New("No video streams found")
	}
	// best video stream
	var bestStream api.AdaptiveFormat
	bestItag := 0
	for _, stream := range video.AdaptiveFormats {
		if !(strings.HasPrefix(stream.Type, "audio/")) {
			continue
		}

		itag, err := strconv.Atoi(stream.Itag)
		if err != nil {
			continue
		}
		if itag > bestItag {
			bestItag = itag
			bestStream = stream
		}
	}

	audioItag := bestStream.Itag
	audioFile := filenameEscape(fmt.Sprintf("%s_%s.%s", video.Title, video.VideoID, bestStream.Container))
	log.Println("Downloading audio...")
	return video.DownloadFile(audioItag, audioFile)
}

func downloadVideo(id string) error {
	video, err := instance.NewVideo(id)
	if err != nil {
		return err
	}

	log.Println(video.Title)

	if len(video.FormatStreams) == 0 {
		return errors.New("No video streams found")
	}
	// best video stream
	bestStream := video.FormatStreams[0]
	bestItag := 0
	for _, stream := range video.FormatStreams {
		itag, err := strconv.Atoi(stream.Itag)
		if err != nil {
			continue
		}
		if itag > bestItag {
			bestItag = itag
			bestStream = stream
		}
	}

	videoItag := bestStream.Itag
	videoFile := filenameEscape(fmt.Sprintf("%s_%s.%s", video.Title, video.VideoID, bestStream.Container))
	log.Println("Downloading video...")
	err = video.DownloadFile(videoItag, videoFile)
	if err != nil {
		return err
	}
	subtitleLangCode := "en"
	srtFile := filenameEscape(fmt.Sprintf("%s_%s.%s.srt", video.Title, video.VideoID, subtitleLangCode))
	log.Println("Downloading subtitle...")
	return video.DownloadSubtitle(subtitleLangCode, srtFile, "srt")
}

// filenameEscape escapes non-valid filename characters from a string
func filenameEscape(filename string) string {
	return strings.ReplaceAll(filename, string(os.PathSeparator), "_")
}
